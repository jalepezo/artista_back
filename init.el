;; Define custom.el that contains custom variables apart from init.el
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))
;; ===================================

;; USING PACKAGES

;; ===================================

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;;SETTING UP THE USE PACKAGE 
(require 'use-package)
(setq use-package-always-ensure t)

;; LINE NUMBERING
(column-number-mode)
(global-display-line-numbers-mode t)

;ivy mode
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")

;;ADD EXCEPTIONS TO LINE NUMEBERING

(dolist (mode '(org-mode-hook
		shell-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; EXTERNAL THEMES FOLDER

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'cyberpunk t)
;; ===================================

;; Basic Customization

;; ===================================

(use-package rainbow-delimiters
  :hook(prog-mode . rainbow-delimiters-mode)) ;;Parentheses 

(setq inhibit-startup-message t)    ;; Hide the startup message

(use-package which-key              ;;WHICH KEY
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

;; ====================================

;; Development Setup

;; ====================================
;;LSP MODE
(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (python-mode . lsp)
	 (R-mode . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)


;; optionally if you want to use debugger
(use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language
;;lsp-ivy
(use-package lsp-ivy)


;;PYTHON

;;PYLSP INSTALLED FROM PIP

;;elpy

(use-package elpy
  :ensure t
  :init
  (elpy-enable))


;;FLYCHECK

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))
  
;; Auto completion
(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 2)
  (global-company-mode t)
  )
;; Parentheses
(use-package highlight-parentheses
  :ensure t
  :config
  (progn
    (highlight-parentheses-mode)
    (global-highlight-parentheses-mode))
  )
;;;===========================================

;; ORGMODE

;;;============================================

(use-package org)

;;ORG-AGENDA
(global-set-key (kbd "C-C a") 'org-agenda)
(setq org-agenda-files
      '("~/Documents/job/control_job.org"))
(setq org-agenda-start-with-log-mode t)



;;ORG ROAM
(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/roamNotes")
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert))
  :config
  (org-roam-setup))

;; ORG-REF

(use-package org-ref)

(setq bibtex-completion-bibliography '("~/Documents/job/presentaciones/unsaac_abril/ref.bib"))

(require 'bibtex)

(setq bibtex-autokey-year-length 4
      bibtex-autokey-name-year-separator "-"
      bibtex-autokey-year-title-separator "-"
      bibtex-autokey-titleword-separator "-"
      bibtex-autokey-titlewords 2
      bibtex-autokey-titlewords-stretch 1
      bibtex-autokey-titleword-length 5)

(define-key bibtex-mode-map (kbd "H-b") 'org-ref-bibtex-hydra/body)

(require 'org-ref-ivy)

(setq org-ref-insert-link-function 'org-ref-insert-link-hydra/body
      org-ref-insert-cite-function 'org-ref-cite-insert-ivy
      org-ref-insert-label-function 'org-ref-insert-label-link
      org-ref-insert-ref-function 'org-ref-insert-ref-link
      org-ref-cite-onclick-function (lambda (_) (org-ref-citation-hydra/body)))
(define-key org-mode-map (kbd "C-c ]") 'org-ref-insert-link)

;;pdf tools

(pdf-loader-install)

;; ORG-ROAM-BIBTEX

;(use-package org-roam-bibtex
 ; :after org-roam)



;; required for compilation in org src blocks & tangle
;;(require 'ob-sqlite)
;;(require 'ob-sql)
;;(require 'ob-emacs-lisp)
;;(require 'ob-R)
;;(require 'ob-C)
;;(require 'ob-shell)

;; active Babel languages for ORG code snippets
;;(org-babel-do-load-languages
;; 'org-babel-load-languages
;; '((R . t)
  ;; (sql . t)
  ;; (python . t)
   ;;(emacs-lisp . t)
   ;;(C . t)))

;; Syntax highlight code in your SRC blocks The last variable removes the annoying “Do you want to execute?”

;;(setq org-confirm-babel-evaluate nil
  ;;    org-src-fontify-natively t
    ;;  org-src-tab-acts-natively t)


;;SPELLING IN SPANISH AND ENGLISH
(with-eval-after-load "ispell"
  ;; Configure `LANG`, otherwise ispell.el cannot find a 'default
  ;; dictionary' even though multiple dictionaries will be configured
  ;; in next line.
  (setenv "LANG" "en_US.UTF-8")
  (setq ispell-program-name "hunspell")
  ;; Configure SPANISH AND AMERICAN ENGLISH
  (setq ispell-dictionary "es_PE,en_US")
  ;; ispell-set-spellchecker-params has to be called
  ;; before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "es_PE,en_US")
  ;; For saving words to the personal dictionary, don't infer it from
  ;; the locale, otherwise it would save to ~/.hunspell_de_DE.
  (setq ispell-personal-dictionary "~/.hunspell_personal"))
;; The personal dictionary file has to exist, otherwise hunspell will
;; silently not use it.

