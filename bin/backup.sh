#!/bin/bash
#author: Gonzalo Ale
#email: jalepezo@gmail.com
#script to choose files to back up
#the files will be form the user's HOME
#directory and will be backed up in HOME
#last edited: 01-04-2020
read -p "which file types do you want to backup " file_suffix
read -p "Which directory do you want to backup to " dir_name
#we will create the directory if it does no exists
test -d $HOME/$dir_name || mkdir -m 700 $HOME/$dir_name
#the find command will copyu files with desired extension. The -path. -prune and -o options exclude the backed directory from the backup
find $HOME -path $HOME/$dir_name -prune -o \
     -name "*$file_suffix" -exec cp {} $HOME/$dir_name/ \;
exit 0 
