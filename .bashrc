
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

alias sus='systemctl suspend'
alias z='zathura'
alias po='poweroff'

PS1='[\u@\h \W]\$ '

export PATH=$PATH:~/bin
export PATH=$PATH:/home/chalo/.local/bin
export EDITOR=vim

if [ -n $R_LIBS ]; then
	  export R_LIBS=~/Rlibs:$R_LIBS
  else
	    export R_LIBS=~/Rlibs
fi
